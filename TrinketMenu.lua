local AS = unpack(AddOnSkins)

if not AS:CheckAddOn('TrinketMenu') then return end

function AS:TrinketMenu()
	AS:SkinIconButton(TrinketMenu_Trinket0)
	AS:SkinIconButton(TrinketMenu_Trinket1)
	AS:SkinFrame(TrinketMenu_MenuFrame)
	AS:SkinFrame(TrinketMenu_OptFrame)
	--AS:SkinCloseButton(TrinketMenu_CloseButton)
	AS:SkinButton(TrinketMenu_CloseButton)
	AS:SkinButton(TrinketMenu_LockButton)
	AS:SkinBackdropFrame(TrinketMenu_SubOptFrame)

	AS:SkinCheckBox(TrinketMenu_OptLocked)
    AS:SkinCheckBox(TrinketMenu_OptSquareMinimap)
    AS:SkinCheckBox(TrinketMenu_OptCooldownCount)
    AS:SkinCheckBox(TrinketMenu_OptLargeCooldown)
    AS:SkinCheckBox(TrinketMenu_OptShowTooltips)
    AS:SkinCheckBox(TrinketMenu_OptTooltipFollow)
    AS:SkinCheckBox(TrinketMenu_OptTinyTooltips)
    AS:SkinCheckBox(TrinketMenu_OptShowHotKeys)
    AS:SkinCheckBox(TrinketMenu_OptStopOnSwap)
    AS:SkinCheckBox(TrinketMenu_OptRedRange)
    AS:SkinCheckBox(TrinketMenu_OptHidePetBattle)

	AS:SkinCheckBox(TrinketMenu_Trinket0Check)
	AS:SkinCheckBox(TrinketMenu_Trinket1Check)
	AS:SkinCheckBox(TrinketMenu_OptShowIcon)
	AS:SkinCheckBox(TrinketMenu_OptDisableToggle)
	AS:SkinCheckBox(TrinketMenu_OptKeepDocked)
	AS:SkinCheckBox(TrinketMenu_OptKeepOpen)
	AS:SkinCheckBox(TrinketMenu_OptMenuOnShift)
	AS:SkinCheckBox(TrinketMenu_OptMenuOnRight)
	AS:SkinCheckBox(TrinketMenu_OptNotify)
	AS:SkinCheckBox(TrinketMenu_OptNotifyThirty)
	AS:SkinCheckBox(TrinketMenu_OptNotifyChatAlso)
	AS:SkinCheckBox(TrinketMenu_OptSetColumns)

	AS:SkinSlideBar(TrinketMenu_OptMainScaleSlider)
	AS:SkinSlideBar(TrinketMenu_OptMenuScaleSlider)
	AS:SkinSlideBar(TrinketMenu_OptColumnsSlider)

	for i = 1, 3 do
		AS:SkinTab(_G["TrinketMenu_Tab"..i])
	end

	for i = 1, 9 do
		AS:SkinIconButton(_G["TrinketMenu_Menu"..i])
	end
	-- testing
	--AS:SkinIcon(TrinketMenu_Trinket1Queue)
	--AS:SkinIcon(TrinketMenu_Trinket0Queue)
end

AS:RegisterSkin('TrinketMenu', AS.TrinketMenu)
